DoIT fork of [spotseeker_client](https://github.com/uw-it-aca/spotseeker_client)
by the University of Washington Information Technology's Academic Experience Design & Delivery group.
Major changes are from upstream project is this fork has been updated Python 3.x and Django 2.x.

### Requirement
* Python 3.6.x
* Django 2.x

### Development 
Install spotseeker_client as a Django module in [WiScout](https://git.doit.wisc.edu/andrew-summers/wiscout) Django app.

* Clone WiScout to your machine.

* Install spotseeker_server as an editable dependency.
  
```sh
# from wiscout
$ pipenv install -e /path/to/spotseeker_client
```
  
* Add spotseeker_client to `INSTALLED_APPS`
  
```py
# wiscout/settings.py
INSTALLED_APPS = [
...
'spotseeker_restclient',
....
]
```

### Run Unit Tests
```py
# from wiscout 
$ python manage.py test spotseeker_restclient.test.spot
```